
// named export
export const log = entry => console.log(entry);

export const error = entry => console.error(entry);

export default {
  log, error
};