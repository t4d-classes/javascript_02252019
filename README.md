# Welcome to the ES2018 JavaScript Class

## Instructor

Eric Greene

## Schedule

Class:

- Monnday - Tuesday, 9am to 5pm PST

Breaks:

- Morning: 10:25am to 10:35am
- Lunch: Noon to 1pm
- Afternoon #1: 2:15pm to 2:25pm
- Afternoon #2: 3:40pm to 3:50pm

## Course Outline

- Day 1 - Types, Variables, Functions, Objects, Arrays, Prototype Inheritance
- Day 2 - Asynchronous Programming, Closures, Callbacks, Promises, ES2015 Modules

### Requirements

- Node.js (version 10 or later)
- Web Browser
- Text Editor

### Instructor's Resources

- [DevelopIntelligence](http://www.developintelligence.com/)
